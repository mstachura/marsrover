﻿namespace MarsRover.Core
{
    using System;

    public class PositionFactory
    {
        public static Position Create(string coordinates)
        {
            return ParseCoordinates(coordinates);
        }

        private static Position ParseCoordinates(string coordinates)
        {
            var elements = coordinates
                .Replace("(", string.Empty)
                .Replace(")", string.Empty)
                .Split(',');

            return new Position
            {
                X = int.Parse(elements[0].Trim()), 
                Y = int.Parse(elements[1].Trim()),
                Orientation = (Orientation)Enum.Parse(typeof(Orientation), elements[2].Trim())
            };
        }
    }
}
