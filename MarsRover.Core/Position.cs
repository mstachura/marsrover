﻿namespace MarsRover.Core
{
    public class Position
    {
        public int X { get; set; }
        
        public int Y { get; set; }

        public Orientation Orientation { get; set; }

        public override string ToString()
        {
            return $"({X},{Y},{Orientation})";
        }
    }
}
