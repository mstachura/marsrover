﻿namespace MarsRover.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class PositionCalculator
    {
        private static readonly IReadOnlyCollection<char> MoveCommands = new List<char> { 'F', 'B' };
        
        private static readonly IReadOnlyCollection<char> RotateCommands = new List<char> { 'L', 'R' };


        public Position CalculatePosition(char command, Position currentPosition)
        {
            if (MoveCommands.Contains(command))
            {
                return CalculatePositionBasedOnMoveCommand(command, currentPosition);
            }

            if (RotateCommands.Contains(command))
            {
                return CalculatePositionBasedOnRotateCommand(command, currentPosition);
            }

            throw new ArgumentOutOfRangeException();
        }

        private static Position CalculatePositionBasedOnMoveCommand(char command, Position currentPosition)
        {
            switch (currentPosition.Orientation)
            {
                case Orientation.North:
                    return command == 'F'
                        ? new Position
                            {X = currentPosition.X, Y = currentPosition.Y + 1, Orientation = currentPosition.Orientation}
                        : new Position
                            {X = currentPosition.X, Y = currentPosition.Y - 1, Orientation = currentPosition.Orientation};
                case Orientation.East:
                    return command == 'F'
                        ? new Position
                            {X = currentPosition.X + 1, Y = currentPosition.Y, Orientation = currentPosition.Orientation}
                        : new Position
                            {X = currentPosition.X - 1, Y = currentPosition.Y, Orientation = currentPosition.Orientation};
                case Orientation.South:
                    return command == 'F'
                        ? new Position
                            {X = currentPosition.X, Y = currentPosition.Y - 1, Orientation = currentPosition.Orientation}
                        : new Position
                            {X = currentPosition.X, Y = currentPosition.Y + 1, Orientation = currentPosition.Orientation};
                case Orientation.West:
                    return command == 'F'
                        ? new Position
                            {X = currentPosition.X - 1, Y = currentPosition.Y, Orientation = currentPosition.Orientation}
                        : new Position
                            {X = currentPosition.X + 1, Y = currentPosition.Y, Orientation = currentPosition.Orientation};
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static Position CalculatePositionBasedOnRotateCommand(char command, Position currentPosition)
        {
            switch (currentPosition.Orientation)
            {
                case Orientation.North:
                    return command == 'L'
                        ? new Position {X = currentPosition.X, Y = currentPosition.Y, Orientation = Orientation.West}
                        : new Position {X = currentPosition.X, Y = currentPosition.Y, Orientation = Orientation.East};
                case Orientation.East:
                    return command == 'L'
                        ? new Position {X = currentPosition.X, Y = currentPosition.Y, Orientation = Orientation.North}
                        : new Position {X = currentPosition.X, Y = currentPosition.Y, Orientation = Orientation.South};
                case Orientation.South:
                    return command == 'L'
                        ? new Position {X = currentPosition.X, Y = currentPosition.Y, Orientation = Orientation.East}
                        : new Position {X = currentPosition.X, Y = currentPosition.Y, Orientation = Orientation.West};
                case Orientation.West:
                    return command == 'L'
                        ? new Position {X = currentPosition.X, Y = currentPosition.Y, Orientation = Orientation.South}
                        : new Position {X = currentPosition.X, Y = currentPosition.Y, Orientation = Orientation.North};
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
