﻿namespace MarsRover.Core
{
    public class MarsRover
    {
        private readonly PositionCalculator _positionCalculator;
        private Position _position;

        public MarsRover(string coordinates, PositionCalculator positionCalculator)
        {
            _positionCalculator = positionCalculator;
            _position = PositionFactory.Create(coordinates);
        }

        public string Move(string commands)
        {
            foreach (var command in commands)
            {
                _position = _positionCalculator.CalculatePosition(command, _position);
            }

            return _position.ToString();
        }
    }
}
