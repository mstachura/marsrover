﻿namespace MarsRover.App
{
    using System;
    using MarsRover.Core;

    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("You have not provided valid number of arguments.");
                Console.WriteLine("Please provide two arguments, initial position and command to move.");
                Console.WriteLine("Eg.: (0,0,North) FFLRBF");
                return;
            }

            var initialPosition = args[0];
            var command = args[1];
            var marsRover = new MarsRover(initialPosition, new PositionCalculator());
            var currentPosition = marsRover.Move(command);

            Console.WriteLine("Yay, you have successfully moved Mars Rover!");
            Console.WriteLine($"Rover's current position: {currentPosition}");
        }
    }
}
