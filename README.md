# Mars Rover App

## Getting started

* make sure to have installed .NET Core 3.1 Framework (https://dotnet.microsoft.com/download/dotnet-core/3.1) on your machine.

* clone the respository:

    ```
    git clone git@bitbucket.org:mstachura/marsrover.git
    ```

* run the application

    On Windows system:
    
    ```
    dotnet run --project MarsRover.App\MarsRover.App.csproj -- (4,2,East) FLFFFRFLB
    ```


    On Linux based system:
    
    ```
    dotnet run --project MarsRover.App/MarsRover.App.csproj -- "(4,2,East)" FLFFFRFLB
    ```


## Things to be improved

* user input validation and tests for it
* Further refactoring of PostitionCalculator
