﻿namespace MarsRover.Tests
{
    using Xunit;
    using FluentAssertions;
    using MarsRover.Core;

    public class MarsRoverRotatingTests
    {
        [Theory]
        [InlineData("(0,0,North)", "L", "(0,0,West)")]
        [InlineData("(0,0,East)", "L", "(0,0,North)")]
        [InlineData("(0,0,South)", "L", "(0,0,East)")]
        [InlineData("(0,0,West)", "L", "(0,0,South)")]
        [InlineData("(0,0,North)", "R", "(0,0,East)")]
        [InlineData("(0,0,East)", "R", "(0,0,South)")]
        [InlineData("(0,0,South)", "R", "(0,0,West)")]
        [InlineData("(0,0,West)", "R", "(0,0,North)")]
        public void ItShouldRotate(string initialCoordinates, string command, string expectedCoordinates)
        {
            var subject = SetupMarsRover(initialCoordinates);

            var result = subject.Move(command);

            result.Should().Be(expectedCoordinates);
        }

        [Theory]
        [InlineData("(0,0,North)", "LR", "(0,0,North)")]
        [InlineData("(0,0,East)", "LR", "(0,0,East)")]
        [InlineData("(0,0,South)", "LR", "(0,0,South)")]
        [InlineData("(0,0,West)", "LR", "(0,0,West)")]
        [InlineData("(0,0,North)", "RL", "(0,0,North)")]
        [InlineData("(0,0,East)", "RL", "(0,0,East)")]
        [InlineData("(0,0,South)", "RL", "(0,0,South)")]
        [InlineData("(0,0,West)", "RL", "(0,0,West)")]
        public void ItShouldRotateLeftAndRight(string initialCoordinates, string command, string expectedCoordinates)
        {
            var subject = SetupMarsRover(initialCoordinates);

            var result = subject.Move(command);

            result.Should().Be(expectedCoordinates);
        }

        private static MarsRover SetupMarsRover(string initialCoordinates)
        {
            var subject = new MarsRover(initialCoordinates, new PositionCalculator());
            return subject;
        }
    }
}
