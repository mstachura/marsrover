namespace MarsRover.Tests
{
    using Xunit;
    using FluentAssertions;
    using MarsRover.Core;

    public class MarsRoverMovingTests
    {
        [Theory]
        [InlineData("(0,0,North)", "F", "(0,1,North)")]
        [InlineData("(0,0,North)", "B", "(0,-1,North)")]
        [InlineData("(0,0,East)", "F", "(1,0,East)")]
        [InlineData("(0,0,East)", "B", "(-1,0,East)")]
        [InlineData("(0,0,South)", "F", "(0,-1,South)")]
        [InlineData("(0,0,South)", "B", "(0,1,South)")]
        [InlineData("(0,0,West)", "F", "(-1,0,West)")]
        [InlineData("(0,0,West)", "B", "(1,0,West)")]
        public void ItShouldMoveSingleSteps(string initialCoordinates, string command, string expectedCoordinates)
        {
            var subject = SetupMarsRover(initialCoordinates);

            var result = subject.Move(command);

            result.Should().Be(expectedCoordinates);
        }

        [Theory]
        [InlineData("(0,0,North)", "FF", "(0,2,North)")]
        [InlineData("(0,0,East)", "FF", "(2,0,East)")]
        [InlineData("(0,0,South)", "FF", "(0,-2,South)")]
        [InlineData("(0,0,West)", "FF", "(-2,0,West)")]
        [InlineData("(0,0,North)", "BB", "(0,-2,North)")]
        [InlineData("(0,0,East)", "BB", "(-2,0,East)")]
        [InlineData("(0,0,South)", "BB", "(0,2,South)")]
        [InlineData("(0,0,West)", "BB", "(2,0,West)")]
        public void ItShouldMoveMultipleSteps(string initialCoordinates, string command, string expectedCoordinates)
        {
            var subject = SetupMarsRover(initialCoordinates);

            var result = subject.Move(command);

            result.Should().Be(expectedCoordinates);
        }

        [Theory]
        [InlineData("(0,1,North)", "FB", "(0,1,North)")]
        [InlineData("(1,0,North)", "BF", "(1,0,North)")]
        [InlineData("(-1,3,East)", "FB", "(-1,3,East)")]
        [InlineData("(3,-1,East)", "BF", "(3,-1,East)")]
        [InlineData("(-5,-3,South)", "FB", "(-5,-3,South)")]
        [InlineData("(4,4,South)", "BF", "(4,4,South)")]
        [InlineData("(-5,5,West)", "FB", "(-5,5,West)")]
        [InlineData("(7,-4,West)", "BF", "(7,-4,West)")]
        public void ItShouldMoveForwardAndBackwards(string initialCoordinates, string command, string expectedCoordinates)
        {
            var subject = SetupMarsRover(initialCoordinates);

            var result = subject.Move(command);

            result.Should().Be(expectedCoordinates);
        }

        private static MarsRover SetupMarsRover(string initialCoordinates)
        {
            var subject = new MarsRover(initialCoordinates, new PositionCalculator());
            return subject;
        }
    }
}
