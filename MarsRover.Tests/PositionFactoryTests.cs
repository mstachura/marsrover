﻿namespace MarsRover.Tests
{
    using FluentAssertions;
    using MarsRover.Core;
    using Xunit;

    public class PositionFactoryTests
    {
        [Theory]
        [InlineData("(0,0,North)", 0, 0, Orientation.North)]
        [InlineData("(1,-1,East)", 1, -1, Orientation.East)]
        [InlineData("(-10,5,South)", -10, 5, Orientation.South)]
        [InlineData("(-3,-4,West)", -3, -4, Orientation.West)]
        public void ItShouldCorrectlyParseCoordinates(string coordinates, int x, int y, Orientation orientation)
        {
            var position = PositionFactory.Create(coordinates);

            position.Should().BeEquivalentTo(new Position {X = x, Y = y, Orientation = orientation});
        }
    }
}
