﻿namespace MarsRover.Tests
{
    using Xunit;
    using FluentAssertions;
    using MarsRover.Core;

    public class MarsRoverTests
    {

        [Theory]
        [InlineData("(4,2,East)", "FLFFFRFLB", "(6,4,North)")]
        public void ItShouldMoveToExpectedPosition(string initialCoordinates, string command, string expectedCoordinates)
        {
            var subject = new MarsRover(initialCoordinates, new PositionCalculator());

            var result = subject.Move(command);

            result.Should().Be(expectedCoordinates);
        }
    }
}
